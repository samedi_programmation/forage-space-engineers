public Program()
{
    Runtime.UpdateFrequency = UpdateFrequency.Once;
}

public void Save()
{

}

public void ExtendPiston(IMyPistonBase piston)
{
       piston.Velocity = 0.01f;
       piston.Extend();  
}

public void RetractPiston(IMyPistonBase piston)
{
       piston.Velocity = 1.0f;
       piston.Retract();  
}

List<IMyPistonBase> GetAllPistons()
{
    List<IMyTerminalBlock> allBlocks = new List<IMyTerminalBlock>(); 
    GridTerminalSystem.GetBlocksOfType<IMyPistonBase>(allBlocks); // Where allBlocks is a list type.  

    return allBlocks.ConvertAll(x => (IMyPistonBase)x);
}

public void RetractAllPistons() {
    List<IMyPistonBase> allPistons = GetAllPistons();
    foreach(IMyPistonBase piston in allPistons) {
        RetractPiston(piston);
    }
}

public void ExtendAllPistons() {
    List<IMyPistonBase> allPistons = GetAllPistons();
    foreach(IMyPistonBase piston in allPistons) {
        ExtendPiston(piston);
    }
}

List<IMyShipDrill> GetAllDrills()
{
    List<IMyTerminalBlock> allBlocks = new List<IMyTerminalBlock>(); 
    GridTerminalSystem.GetBlocksOfType<IMyShipDrill>(allBlocks); // Where allBlocks is a list type.  

    return allBlocks.ConvertAll(x => (IMyShipDrill)x);
}

bool AreDrillsRunning() {
    bool result = true;

    List<IMyShipDrill> allDrills = GetAllDrills();
    foreach(IMyShipDrill drill in allDrills) {
        
        result = result & drill.Enabled;

    }
    return result;
}

public void RunDrills(bool enabled) {
    List<IMyShipDrill> allDrills = GetAllDrills();
    foreach(IMyShipDrill drill in allDrills) {
        drill.Enabled = enabled;
    }
}

public void Main(string argument, UpdateType updateSource)
{

//    IMyShipDrill DrillRight = GridTerminalSystem.GetBlockWithName("ForeuseDroite") as IMyShipDrill;
//    IMyShipDrill DrillLeft = GridTerminalSystem.GetBlockWithName("ForeuseGauche") as IMyShipDrill;

    Echo(GetAllPistons().Count + " pistons trouvés");
    Echo(GetAllDrills().Count + " foreuses trouvées");

    IMyPistonBase Piston1 = GridTerminalSystem.GetBlockWithName("Piston_1") as IMyPistonBase;
//    IMyPistonBase Piston2 = GridTerminalSystem.GetBlockWithName("Piston_2") as IMyPistonBase;
//    IMyPistonBase Piston3 = GridTerminalSystem.GetBlockWithName("Piston_3") as IMyPistonBase;
//    IMyPistonBase Piston4 = GridTerminalSystem.GetBlockWithName("Piston_4") as IMyPistonBase;
    
    if (!AreDrillsRunning()) {
        RunDrills(true);

//        DrillRight.Enabled = true;
//        DrillLeft.Enabled = true;
//        ExtendPiston(Piston1);
//        ExtendPiston(Piston2);
//        ExtendPiston(Piston3);
//        ExtendPiston(Piston4);
        ExtendAllPistons();
      Runtime.UpdateFrequency = UpdateFrequency.Update100;
    }

// Retracted, Extending, Extended

    if (Piston1.Status.ToString() == "Extended") {
//        DrillRight.Enabled = false;
//        DrillLeft.Enabled = false;

        RunDrills(false);


//        RetractPiston(Piston1);
//        RetractPiston(Piston2);
//        RetractPiston(Piston3);
//        RetractPiston(Piston4);
        RetractAllPistons();
        Runtime.UpdateFrequency = UpdateFrequency.None;
    }

    
}
