############################
# Les Samedi Programmation #
############################


Samedi 12/09/2020 : Scripting Space Engineers
=============================================


Ce code source est la base du live sur le forage assisté dans Space Engineers.


Il montre un exemple de forage amélioré par pistons, tous gérés par un script.

- Récupération d'objets dans le script
- Interactions
- Listes d'objets de même type


Assistance au scripting :

https://github.com/malware-dev/MDK-SE/wiki/Quick-Introduction-to-Space-Engineers-Ingame-Scripts
https://github.com/malware-dev/MDK-SE/wiki/Api-Index


Si vous avez plus de questions, vous pouvez me contacter sur https://twitch.tv/adaralex

Bonne journée à vous,

Adaralex